import { db } from './firebase';
const reference = db.ref("msgs");
const putMessage=(msg)=>{
    reference.push().set(msg);
};

const getMsgs=()=>{
    var arr=[];
    reference.on('value',function(snapshot){
        arr.concat(snapshot.val());
    });
    console.log(arr);
    return arr;
};

export{
  putMessage,
  getMsgs,
  reference,
}
