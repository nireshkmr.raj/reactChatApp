import { auth } from './firebase';

const createUser=(email,password)=>{
    return auth.createUserWithEmailAndPassword(email,password);
};

const signInUser =(email,password)=>{
  return auth.signInWithEmailAndPassword(email,password);
};

  const signOut = ()=>{
  auth.signOut();
};
const authState=(x)=>{
  return auth.onAuthStateChanged(x);
}

export {
  createUser,
  signInUser,
  signOut,
  authState,
}
