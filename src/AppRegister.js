import React,{Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import InputAdornment from '@material-ui/core/InputAdornment';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CardHeader from '@material-ui/core/CardHeader';
import lightBlue from '@material-ui/core/colors/lightBlue';
import blue from '@material-ui/core/colors/blue';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Snackbar from '@material-ui/core/Snackbar';
import { auth } from './firebaseFiles/index';
import { withRouter } from 'react-router-dom';

class AppRegister extends Component{

  constructor(props){
    super(props)
    this.state={"email":"","password":"","showPassword":false,"open":false,"msg":""};
    this.signInHandler=this.signInHandler.bind(this);
    this.signUpHandler=this.signUpHandler.bind(this);
    this.handleEmailChange =this.handleEmailChange.bind(this);
    this.handlePasswordChange =this.handlePasswordChange.bind(this);
    this.handleClickShowPassword =this.handleClickShowPassword.bind(this);
    this.hideSnackBar= this.hideSnackBar.bind(this);
  }

  signInHandler(){
    const emailText = this.state.email;
    const passwordText = this.state.password;
    if(emailText===""||passwordText===""){
      this.setState({
        open:true,
        msg:"Fill all the details",
      });
      return;
    }
    this.setState({
      email:"",
      password:""
    });
    auth.signInUser(emailText,passwordText)
      .then((authUser)=>{
        console.log("in");
                this.props.history.push("/chat");
      })
      .catch((error)=>{
                    var errorCode=error.code;
                    console.log(errorCode);
                    if(errorCode==="auth/wrong-password"){
                      this.setState({
                          open:true,
                          msg:"Wrong Password",
                      });
                    }else if(errorCode==="auth/invalid-email"){
                      this.setState({
                          open:true,
                          msg:"Invalid Email",
                      });
                      }
        });
      }

  signUpHandler(){
    const emailText = this.state.email;
    const passwordText = this.state.password;
    if(emailText===""||passwordText===""){
      this.setState({
        open:true,
        msg:"Fill all the details",
      });
      return;
    }
    this.setState({
      email:"",
      password:""
    });
    auth.createUser(emailText,passwordText)
      .then((authUser)=>{
        this.props.history.push("/chat");
      })
      .catch((error)=>{
              var errorCode=error.code;
                if(errorCode==="auth/invalid-email"){
                  this.setState({
                      open:true,
                      msg:"Invalid Email",
                  });
                }else if(errorCode==="auth/email-already-in-use"){
                  this.setState({
                      open:true,
                      msg:"Email Already In Use",
                  });
                }else if(errorCode==="auth/weak-password"){
                  this.setState({
                      open:true,
                      msg:"Password should be more than 6 characters.",
                  });
                }
    });
  }

  handleEmailChange(event){
    this.setState({
      email:event.target.value
    });
  }

  handlePasswordChange(event){
    this.setState({
      password:event.target.value
    });
  }

  handleClickShowPassword(){
     this.setState({
       showPassword: !this.state.showPassword
     });
  }

  hideSnackBar(){
    this.setState({
      open:false
    });
  }

  render(){

      const styles ={
        card:{
          marginTop:220,
          padding:10,
        },
        cardActions1:{
          marginLeft:'auto',
          color:blue[50],
          backgroundColor:lightBlue[500],
        },
        cardActions2:{
          color:blue[50],
          backgroundColor:lightBlue[500],
        },
      };

      return(
        <div>
          <AppBar color='default'>
              <Toolbar color='inherit'>
                <Typography variant='headline' component='h3'>
                        Authenticate Yourself!
                </Typography>
              </Toolbar>
           </AppBar>
          <Grid container>
                <Grid item md={4} sm={4} xs={1}></Grid>
                <Grid item md={4} sm={4} xs={10}>
                      <Card style={styles.card}>

                            <CardHeader title='Authenticate'></CardHeader>
                                    <CardContent spacing={8}>
                                        <InputLabel htmlFor="email">Email</InputLabel>
                                        <Input     id="email"
                                                   type='text'
                                                   value={this.state.email}
                                                   fullWidth
                                                   onChange={this.handleEmailChange}/>

                                        <InputLabel htmlFor="password">Password</InputLabel>
                                        <Input     id="password"
                                                   type={this.state.showPassword?'text':'password'}
                                                   value={this.state.password}
                                                   fullWidth
                                                   onChange={this.handlePasswordChange}
                                                   endAdornment={
                                                        <InputAdornment position="end">
                                                            <IconButton onClick={this.handleClickShowPassword}>
                                                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                                            </IconButton>
                                                        </InputAdornment>
                                                  }/>

                                    </CardContent>

                                    <CardActions>
                                        <Button variant="contained" style={styles.cardActions1}  onClick={this.signUpHandler}>SignUp</Button>
                                        <Button variant="contained" style={styles.cardActions2} onClick={this.signInHandler}>SignIn</Button>
                                    </CardActions>

                      </Card>
                </Grid>
                <Grid item md={4} sm={4} xs={1}>
                </Grid>
           </Grid>
          <Snackbar
            open={this.state.open}
            anchorOrigin={ {vertical:'bottom',horizontal:'left'} }
            message={<span>{this.state.msg}</span>}
            onClose={this.hideSnackBar}
            autoHideDuration={1000}
           />
        </div>
      );

  };

}


export default withRouter(AppRegister);
