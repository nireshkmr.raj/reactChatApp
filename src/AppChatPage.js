import React,{Component} from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import PaperContainer from './PaperContainer.js';
import {auth} from './firebaseFiles/index';
import { withRouter } from 'react-router-dom';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';

class AppChatPage extends Component{

    constructor(props){
      super(props)
      this.state={view:false,name:"",open:false};
      this.handleChange = this.handleChange.bind(this);
      this.signOut = this.signOut.bind(this);
      this.handleClose1 = this.handleClose1.bind(this);
      this.handleClose2 = this.handleClose2.bind(this);
    }

    componentDidMount(){
        auth.authState((user)=>{
            if(!user){
              this.props.history.push('/');
              return;
            }
            var sname=user.email;
            var username = "@"+sname.substr(0,sname.indexOf("@"));
            this.setState({
              name:username
            });
        });
    }

    handleChange(){
      this.setState({
        view:this.state.view?false:true
      });
    }

    handleClose2 = () => {
      this.setState({ open: false });
    };

    handleClose1 = () => {
      this.setState({ open: false });
      auth.signOut();
    };

    signOut(){
      this.setState({ open: true });
    }

    render(){
      const styles={
        fab:{
          position:'absolute',
          bottom:0,
          right:0,
          margin:40
        },
        toolbartext:{
          textAlign: 'center',
        },
        iconlogout:{
          position:'absolute',
          right:0
        },
      };
      return(
        <div>
              <AppBar color='default'>
                  <ToolBar color='inherit'>
                  <Typography variant="headline" component="h3" style={styles.toolbartext}>
                        My First React Chat
                  </Typography>
                  <IconButton color='inherit' onClick={this.signOut} style={styles.iconlogout}>
                  <AccountCircle/>
                  </IconButton>
                  </ToolBar>
              </AppBar>
              <Button variant='fab' onClick={this.handleChange} style={styles.fab}  >
                  <AddIcon />
              </Button>
              {
              //<Snackbar open={true} message={this.state.name} autoHideDuration={1000}/>
              }
              <PaperContainer view={this.state.view} name={this.state.name}/>
              <Dialog
                  open={this.state.open}
                  onClose={this.handleClose}
                  >
                   <DialogTitle>{"Are you sure to sign out?"}</DialogTitle>
                   <DialogActions>
                        <Button onClick={this.handleClose1} color="primary" autoFocus>
                          Agree
                        </Button>
                        <Button onClick={this.handleClose2} color="primary">
                          Cancel
                        </Button>
                  </DialogActions>
              </Dialog>
        </div>
      );
    }
}
export default withRouter(AppChatPage);
