import React,{Component} from 'react';
import {BrowserRouter,Route,Link} from 'react-router-dom';

class Testing extends Component{
    render(){
      return(
        <div>
        <BrowserRouter>
          <div>
          <ul>
          <li><Link to="/bike">Bike</Link></li>
          <li><Link to="/car">Car</Link></li>
          </ul>
            <Route exact path="/car" component={Car}/>
            <Route exact path="/bike" component={Bike}/>
          </div>
        </BrowserRouter>
        </div>
      );
    };
}

class Car extends Component{
    render(){
      return(
        <h1>Car</h1>
      );
    }
}

class Bike extends Component{
    render(){
      return(
        <h1>Bike</h1>
      );
    }
}

export default Testing;
