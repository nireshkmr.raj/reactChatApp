import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import './test.css';
import socketIOClient from 'socket.io-client'

class App extends Component {
  constructor(props){
    super(props)
    this.socket=socketIOClient("http://localhost:3001")
    this.state={numbers:[],isOK:true,cname:Math.floor(Math.random() * 10),name:"niersh",message:"hello",temp:"txt"}
    this.handleTextChange = this.handleTextChange.bind(this);
    this.updateValue = this.updateValue.bind(this);
  }
  componentDidMount(){
    this.socket.on('smessage',(data)=>{
      this.setState({
         name: data.name,
         message:data.message
       });  
    })
  }
handleTextChange(evt){
  this.setState({
     temp: evt.target.value
   });
}
  updateValue(){
  this.socket.emit('smessage',{name:this.state.cname,message:this.state.temp})
}
  render() {
    return (
    <div>
    <input type="text" onChange={this.handleTextChange} placeholder="Enter Message" class="messageBox"/>
    <button type="submit" onClick={this.updateValue} class="sendButton">SEND</button>
    <Message /*numbers={this.state.numbers}*/name={this.state.name} message={this.state.message}/>
    </div>
    );
  }
}

class Message extends Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div>
      <p>{this.props.name}</p>
      <p>{this.props.message}</p>
      </div>
    );
  }
}
class MessageList extends Component{
  constructor(props){
    super(props)
    const listItems = props.numbers.map((n)=>{
      return <li><Message key={n.name+n.message} name={n.name} message={n.message}/></li>
    });
    this.state={numbers:props.numbers,values:listItems};
  }
  render(){
    return(
        <ul>
        {
          this.state.values
        }
        </ul>
    );
  }
}



export default App;
