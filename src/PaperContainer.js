import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import React,{Component} from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grow from '@material-ui/core/Grow';
import TextField from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import deepOrange from '@material-ui/core/colors/deepOrange';
import {db} from './firebaseFiles/index';


class PaperContainer extends Component{

  constructor(props){
    super(props)
    this.sendMessage= this.sendMessage.bind(this);
    this.state={msgValue:"",sockets:{},c:[],name:""};
    this.handleTextChange= this.handleTextChange.bind(this);
  }

  componentDidMount(){
      const inst = this;
      db.reference.on('value',function(snapshot){
          var cd=inst.state.c;
          snapshot.forEach(function(childSnapshot) {
            var childData = childSnapshot.val();
            cd=cd.concat(childData);
          });
          inst.setState({
            c:cd
          });
    });
  }

  componentWillReceiveProps(newProps){
    this.setState({
        name:newProps.name
    });
  }

  handleTextChange(event){
    this.setState({
      msgValue:event.target.value
    });
  }
  sendMessage(event){
    const text = this.state.msgValue;
    this.setState({
      msgValue:""
    });
    if(text===""){
        alert("Enter msg");
    }else{
        const data={"name":this.state.name,"msg":text};
        db.putMessage(data);
      }
  }
  render(){
    var styles={};
    if(isWidthUp("sm",this.props.width)){
       styles={
        paper:{
          position:'absolute',
          height:400,
          width:400,
          margin:80,
          bottom:0,
          right:0,
        },
        textfield:{
          position:'absolute',
          bottom:10,
          width:270,
          padding:10
        },
        sendBt:{
          position:'absolute',
          bottom:10,
          margin:10,
          right:0,
        },
      };
    }else{
       styles={
        paper:{
          position:'absolute',
          height:350,
          width:300,
          marginRight:40,
          marginBottom:80,
          bottom:0,
          right:0,
        },
        textfield:{
          position:'absolute',
          bottom:5,
          width:170,
          padding:5
        },
        sendBt:{
          position:'absolute',
          bottom:5,
          margin:5,
          right:0,
        },
      };
    }

      if(this.props.view){
        return(
            <Grow in={this.props.view}
            style={{ transformOrigin: '0 0 0' }}>
              <Paper style={styles.paper} elevation={7}>
                  <ChatList list={this.state.c}/>
                  <TextField
                  value={this.state.msgValue}
                  style={styles.textfield}
                  placeholder="Enter your message"
                  onChange={this.handleTextChange}
                  >
                  </TextField>

                  <Button variant="contained" onClick={this.sendMessage} color="primary" style={styles.sendBt}>
                  Send
                  </Button>


              </Paper>
            </Grow>
        );
      }else{
        return(
          <div></div>
        );
      }
  }
}

function ChatList(props){
  const styles={
 listmain:{
   listStyle:'none',
   maxWidth: 300,
   paddingLeft: 0,
 },
 listElem:{
   position:'relative',
   overflow: 'auto',
   maxWidth: 300,
   maxHeight: 300,
 }
  };
  const vals=props.list.map((x,i)=>{
    return(<ChatItem key={i.toString()} name={x.name} msg={x.msg}/>);
  });
  return(
        <List style={styles.listElem}>
        <ul style={styles.listmain}>
        {
          vals
        }
        </ul>
        </List>
  );
}

function ChatItem(props){
  return(
    <ListItem>
    <Avatar style={{color: '#fff',backgroundColor: deepOrange[500],}}>{props.name.charAt(1)}</Avatar>
    <ListItemText primary={props.name} secondary={props.msg} style={{width:300}}/>
    </ListItem>
  );
}

export default withWidth()(PaperContainer);
