import React, { Component } from 'react';
//import logo from './logo.svg';
import './test.css';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import ToolBar from '@material-ui/core/Toolbar';
import BottomNavigation  from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import  GeoLocation from 'react-geolocation';
import Grid from '@material-ui/core/Grid';

class App extends Component {
  constructor(props){
    super(props)
    this.state={suggestions:[],value:'',selectval:''};
    this.handleChange = this.handleChange.bind(this);
    this.handleBottomChange = this.handleBottomChange.bind(this);
    }
  handleChange=(evt) => {
    console.log(evt.target.value);
    this.setState({
      value:evt.target.value
    });
  }
  handleBottomChange=(evt) => {
    this.setState({
      selectval:evt.target.value
    });
  }
  render() {
    const styles = {
          menuButton: {
            marginLeft: -12,
            marginRight: 20,
          },
          root:{

          },
    };
    const styles2 = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});
    return (
        <div style={styles.root}>
          <AppBar position='static'>
            <ToolBar variant='title' color='inherit'>
              <IconButton style={styles.menuButton} color="inherit" aria-label="Menu">
                {
                   // <MenuIcon onClick={this.handleOpen} />
                }
              </IconButton>
              <Typography variant="headline" component="h2">
                    Title Texts Here!
              </Typography>
            </ToolBar>
          </AppBar>
          <TextField
           fullWidth
           placeholder='Enter a text'
           onChange={this.handleChange}
          />

          <GeoLocation
              lazy
              render={
                  ({
                    fetchingPosition,
                    position: { coords: { latitude, longitude } = {} } = {},
                    error,
                    getCurrentPosition
                  }) =>
                    <div>
                      <Button variant="contained" color="primary" onClick={getCurrentPosition}>Get Position</Button>
                      {
                        error &&
                        <div>
                        {error.message}
                        </div>
                      }
                        <pre>
                        latitude: {latitude}
                        longitude: {longitude}
                        </pre>
                      </div>
                  }
          />
          <Grid container spacing={24}>
      <Grid item xs={6} sm={3}>
        <Paper style={styles2.paper}>xs=6 sm=3</Paper>
        <div>

        </div>
      </Grid>
      <Grid item xs={6} sm={3}>
        <Paper style={styles2.paper}>xs=6 sm=3</Paper>
      </Grid>
      <Grid item xs={6} sm={3}>
        <Paper style={styles2.paper}>xs=6 sm=3</Paper>
      </Grid>
      <Grid item xs={6} sm={3}>
        <Paper style={styles2.paper}>xs=6 sm=3</Paper>
      </Grid>
          </Grid>


          <BottomNavigation
          className="bottomNav"
          onChange={this.handleBottomChange}
          showLabels
          value={this.state.selectval}>
            <BottomNavigationAction label="Menu" icon={<MenuIcon/>}/>
            <BottomNavigationAction label="Restore" icon={<RestoreIcon/>}/>
            <BottomNavigationAction label="Favorite" icon={<FavoriteIcon/>}/>
          </BottomNavigation>

        </div>

    );
  }
}

export default App;
