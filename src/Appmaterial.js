import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import AutoSuggest from 'react-autosuggest';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import ToolBar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';

const suggestions = [
  { label: 'Afghanistan' },
  { label: 'Aland Islands' },
  { label: 'Albania' },
  { label: 'Algeria' },
  { label: 'American Samoa' },
  { label: 'Andorra' },
  { label: 'Angola' },
  { label: 'Anguilla' },
  { label: 'Antarctica' },
  { label: 'Antigua and Barbuda' },
  { label: 'Argentina' },
  { label: 'Armenia' },
  { label: 'Aruba' },
  { label: 'Australia' },
  { label: 'Austria' },
  { label: 'Azerbaijan' },
  { label: 'Bahamas' },
  { label: 'Bahrain' },
  { label: 'Bangladesh' },
  { label: 'Barbados' },
  { label: 'Belarus' },
  { label: 'Belgium' },
  { label: 'Belize' },
  { label: 'Benin' },
  { label: 'Bermuda' },
  { label: 'Bhutan' },
  { label: 'Bolivia, Plurinational State of' },
  { label: 'Bonaire, Sint Eustatius and Saba' },
  { label: 'Bosnia and Herzegovina' },
  { label: 'Botswana' },
  { label: 'Bouvet Island' },
  { label: 'Brazil' },
  { label: 'British Indian Ocean Territory' },
  { label: 'Brunei Darussalam' },
];

function renderInput(inputProps){
  const { ...other } = inputProps;
  return(
    <TextField
     fullWidth
    inputProps={{...other}}
    />
  );
}

function getSuggestion(value){
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;
    return inputLength===0 ? [] :suggestions.filter(suggestion => {
        const keep = count < 5 && suggestion.label.toLowerCase().slice(0, inputLength) === inputValue;
        if (keep) {
          count += 1;
        }
        return keep;
      });
}

function getSuggestionValues(suggestion) {
  return suggestion.label;
}

function renderSuggestionContainer(options){
  const { containerProps, children } = options;
  return(
    <Paper {...containerProps} square>
      {children}
    </Paper>
  );
}

function renderSuggestionBox(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.label, query);
  const parts = parse(suggestion.label, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={String(index)} style={{ fontWeight: 300 }}>
              {part.text}
            </span>
          ) : (
            <strong key={String(index)} style={{ fontWeight: 500 }}>
              {part.text}
            </strong>
          );
        })}
      </div>
    </MenuItem>
  );
}

class App extends Component {
  constructor(props){
    super(props)
    this.state={suggestions:[],value:''};
    this.handleChange = this.handleChange.bind(this);
    this.suggestionclear = this.suggestionclear.bind(this);
    this.suggestionfetch = this.suggestionfetch.bind(this);
  }
  handleChange=(evt) => {
    console.log(evt.target.value);
    this.setState({
      value:evt.target.value
    });
  }
  suggestionclear(){
    this.setState({
      suggestions:[]
    });
  }
  suggestionfetch=({value})=>{
    this.setState({
      suggestions:getSuggestion(value)
    });
  }
  render() {
    const styles = {
          menuButton: {
            marginLeft: -12,
            marginRight: 20,
          },
          titletext:{
            textColor:'#ffffff'
          },
          root:{
            margin:20,
            padding:20
          }
                  };
    return (
        <div>
        {
          // <AppBar position='fixed'>
          //   <ToolBar variant='title' color='inherit'>
          //     <IconButton style={styles.menuButton} color="inherit" aria-label="Menu">
          //         <MenuIcon onClick={this.handleOpen} />
          //     </IconButton>
          //
          //     <Typography variant="headline" component="h2">
          //           Title Texts Here!
          //     </Typography>
          //
          //   </ToolBar>
          // </AppBar>
        }
        <Paper style={styles.root} elevation={6}>
          <Typography variant="headline" component="h3">
              Hello World!
          </Typography>
          <Typography variant="headline" component="h3">
              Hello World!
          </Typography>
          <Typography variant="headline" component="h3">
              Hello World!
          </Typography>
          <Typography variant="headline" component="h3">
              Hello World!
          </Typography>
          <Typography variant="headline" component="h3">
              Hello World!
          </Typography>
          <Typography variant="headline" component="h3">
              Hello World!
          </Typography>
        </Paper>

          <AutoSuggest
          renderInputComponent={renderInput}
          suggestions={this.state.suggestions}
          onSuggestionsFetchRequested={this.suggestionfetch}
          onSuggestionsClearRequested={this.suggestionclear}
          renderSuggestionsContainer={renderSuggestionContainer}
          renderSuggestion={renderSuggestionBox}
          getSuggestionValue={getSuggestionValues}
          inputProps={{
              onChange:this.handleChange,
              placeholder:"Enter input",
              value:this.state.value
          }
          }
          />

        </div>

    );
  }
}

export default App;
