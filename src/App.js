import React,{Component} from 'react';
import AppChatPage from './AppChatPage';
import AppRegister from './AppRegister';
import Testing from './Testing';
import {BrowserRouter, Route } from 'react-router-dom';

class App extends Component{

render(){
  return(
    <BrowserRouter>
    <div>
      <Route exact path="/" component={AppRegister}/>
      <Route exact path="/chat" component={AppChatPage}/>
      <Route exact path="/test" component={Testing}/>
      </div>
    </BrowserRouter>
  );
}

}
export default App;
